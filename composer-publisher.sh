#!/bin/bash
set -o pipefail
# Variables de entorno entrantrantes
# GITEA_USER, GITEA_TOKEN, GIT_URL, GIT_TAG

GITEA_BASE_URL='example.com'

# Variables de entorno
GITEA_USER=$GITEA_USER          # Nombre de usuario de Gitea
GITEA_TOKEN=$GITEA_TOKEN        # Token de acceso de Gitea
GIT_URL=$POST_GIT_URL           # URL del repositorio Git - viene del plugin Webhook Trigger
GIT_TAG=$POST_GIT_TAG           # Etiqueta o versión del repositorio - viene del plugin Webhook Trigger
GITEA_BASE_URL=$GITEA_BASE_URL  # Url base de Gitea

echo -e "GITEA_USER=$GITEA_USER - GIT_URL=$GIT_URL - GIT_TAG=$GIT_TAG \n"

# Codificar las credenciales de Gitea en base64 para la autenticación de la API
GITEA_API_TOKEN=$(echo -n "${GITEA_USER}:${GITEA_TOKEN}" | base64 -w 0)
# Obtener el nombre de la organización de Gitea del URL del repositorio y convertirlo a minúsculas
GITEA_ORGANITATION=$(echo "$GIT_URL" | sed -n 's|.*/\([^/]*\)/[^/]*$|\1|p' | tr '[:upper:]' '[:lower:]')
# Obtener el nombre del proyecto del URL del repositorio y convertirlo a minúsculas
PROYECT_NAME=$(echo "$GIT_URL" | sed -n 's|.*/\([^/]*\)\.git|\1|p' | tr '[:upper:]' '[:lower:]')

# URL de descarga del archivo ZIP desde la API de Gitea
GITEA_API_DOWNLOAD="https://${GITEA_BASE_URL}/api/v1/repos/${GITEA_ORGANITATION}/${PROYECT_NAME}/archive/${GIT_TAG}.zip"
# Ruta y nombre de archivo donde se descargará el ZIP
FILE_NAME="/tmp/${GITEA_ORGANITATION}_${PROYECT_NAME}_${GIT_TAG}.zip"

echo -e "FILE_NAME=$FILE_NAME - GITEA_API_DOWNLOAD=$GITEA_API_DOWNLOAD \n"

# Descargar el archivo ZIP desde la URL de descarga
curl -X 'GET' $GITEA_API_DOWNLOAD -H 'accept: application/json' -H "authorization: Basic ${GITEA_API_TOKEN}" \
--output "${FILE_NAME}"

# Obtener el número de bytes en el archivo ZIP utilizando el comando wc
CHECK_SIZE=$(wc -c < "$FILE_NAME")
MIN_SIZE=200            # Tamaño mínimo en bytes

# Comprobar si el tamaño del archivo es superior al mínimo especificado
if [ "$CHECK_SIZE" -gt "$MIN_SIZE" ]; then
    echo "El archivo se descargo correctamente."
else
    echo "El tamaño del archivo ZIP no cumple el requisito mínimo de $MIN_SIZE bytes."
    # Eliminar el archivo ZIP
    rm -f ${FILE_NAME}
    exit 1
fi

# Subir el archivo ZIP al repositorio de paquetes Gitea utilizando la API
curl --user ${GITEA_USER}:${GITEA_TOKEN} --upload-file "${FILE_NAME}" \
"https://${GITEA_BASE_URL}/api/packages/${GITEA_ORGANITATION}/composer?version=${GIT_TAG}"

# Eliminar el archivo ZIP
rm -f ${FILE_NAME}
